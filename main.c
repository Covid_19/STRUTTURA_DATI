#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char cognome[30];
    char nome[20];
    int eta;
    float stipendio;
} persona;

void carica (persona* arr, int dim);
void stampa (persona* arr, int dim);

int main() {
    persona* arr;
    int dim;
    printf("Quante persone vuoi inserire?\n");
    scanf("%d", &dim);
    arr = (persona*) malloc(sizeof(persona)*dim);
    carica(arr, dim);
    stampa(arr, dim);
    return 0;
}
void carica (persona* arr, int dim){
    for (int i = 0; i<dim; i++){
        printf("\nInserisci il cognome:\n");
        scanf("%s", arr[i].cognome);
        printf("Inserisci il nome:\n");
        scanf("%s", arr[i].nome);
        printf("Inserisci l'età:\n");
        scanf("%d", &arr[i].eta);
        printf("Inserisci lo stipendio:\n");
        scanf("%f", &arr[i].stipendio);
    }
}
void stampa(persona* arr, int dim){
    printf("\nLISTA CON LE PERSONE:\n\n");
    for (int i = 0; i<dim; i++){
        printf("%s    %s   %d   %f\n", arr[i].cognome, arr[i].nome, arr[i].eta, arr[i].stipendio);
    }
}
